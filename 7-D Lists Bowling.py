# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
n, k = [int(s) for s in input().split()]
pins = ['I'] * n
for i in range(k):
  izquierda, derecha = [int(s) for s in input().split()]
  for j in range(izquierda - 1, derecha):
    pins[j] = '.'
print(''.join(pins))